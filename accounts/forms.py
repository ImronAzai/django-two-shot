from django.forms import ModelForm, PasswordInput
from accounts.models import Account, SignUp


class AccountForm(ModelForm):
    class Meta:
        model = Account
        fields = [
            "username",
            "password",
        ]
        widgets = {"password": PasswordInput}


class SignUpForm(ModelForm):
    class Meta:
        model = SignUp
        fields = [
            "username",
            "password",
            "password_confirmation",
        ]
        widgets = {
            "password": PasswordInput,
            "password_confirmation": PasswordInput,
        }
