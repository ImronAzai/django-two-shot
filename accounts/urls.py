from django.urls import path
from .views import show_loginform, logout_user, signup

urlpatterns = [
    path("login/", show_loginform, name="login"),
    path("logout/", logout_user, name="logout"),
    path("signup/", signup, name="signup"),
]
