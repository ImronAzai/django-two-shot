from django.shortcuts import render, redirect

# will need to import redirect later
from .models import Receipt, ExpenseCategory, Account
from django.contrib.auth.decorators import login_required
from .forms import ReceiptForm, ExpenseCategoryForm, AccountForm

# make a view that will get all items in receipt and display them with the con
# look at references for how to make a view_list view


@login_required
def create_receipt(request):
    if request.method == "POST":
        form = ReceiptForm(request.POST)
        if form.is_valid():
            receipt = form.save(False)
            receipt.purchaser = request.user
            receipt.save()
            return redirect("home")
    else:
        form = ReceiptForm
    context = {"form": form}
    return render(request, "sites/create.html", context)


# This function allows users to create a receipt unqiue to them


@login_required
def create_category(request):
    if request.method == "POST":
        form = ExpenseCategoryForm(request.POST)
        if form.is_valid():
            category = form.save(False)
            category.owner = request.user
            category.save()
            return redirect("category_list")
    else:
        form = ExpenseCategoryForm()
    context = {"form": form}
    return render(request, "categories/create.html", context)


@login_required
def create_account(request):
    if request.method == "POST":
        form = AccountForm(request.POST)
        if form.is_valid():
            account = form.save(False)
            account.owner = request.user
            account.save()
            return redirect("account_list")
    else:
        form = AccountForm()
    context = {"form": form}
    return render(request, "accounts/create.html", context)


@login_required
def show_receipts(request):
    receipts_list = Receipt.objects.filter(purchaser=request.user)
    context = {"receipts_list": receipts_list}
    return render(request, "sites/home.html", context)


@login_required
def show_expense_category(request):
    category_list = ExpenseCategory.objects.filter(owner=request.user)
    context = {"category_list": category_list}
    return render(request, "categories/list.html", context)


# this view function above shows the categories of a user @receipts/categories


@login_required
def show_account(request):
    account_list = Account.objects.filter(owner=request.user)
    context = {"account_list": account_list}
    return render(request, "accounts/list.html", context)


# this view function above shows the account of a user @receipts/accounts
