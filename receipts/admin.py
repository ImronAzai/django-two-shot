from django.contrib import admin
from .models import ExpenseCategory, Account, Receipt


class ExpenseCategoryAdmin(admin.ModelAdmin):
    list_display = ["name"]


class AccountAdmin(admin.ModelAdmin):
    list_display = ["name"]


class ReceiptAdmin(admin.ModelAdmin):
    list_display = [
        "vendor",
        "purchaser",
        "category",
        "account",
    ]


admin.site.register(ExpenseCategory, ExpenseCategoryAdmin)


admin.site.register(Account, AccountAdmin)


admin.site.register(Receipt, ReceiptAdmin)
